package main

import "fmt"

func main() {
	// 例如
	// var a int = 10
	// var b = 10
	// c := 10
	//fmt.Println("Hello, World!")
	//println(a, b, c)

	//var a1 = "菜鸟教程"
	// var b1 string = "runoob.com"
	// var c1 bool
	// println(a1, b1, c1)
	//

	// var a int = 21
	// var b int = 10
	// var c int

	// c = a + b
	// fmt.Printf("第一行 - c 的值为 %d\n", c )
	// c = a - b
	// fmt.Printf("第二行 - c 的值为 %d\n", c )
	// c = a * b
	// fmt.Printf("第三行 - c 的值为 %d\n", c )
	// c = a / b
	// fmt.Printf("第四行 - c 的值为 %d\n", c )
	// c = a % b
	// fmt.Printf("第五行 - c 的值为 %d\n", c )
	// a++
	// fmt.Printf("第六行 - a 的值为 %d\n", a )
	// a=21   // 为了方便测试，a 这里重新赋值为 21
	// a--
	// fmt.Printf("第七行 - a 的值为 %d\n", a )

	// const k = 10
	// fmt.Printf("第七行 - k 的值为 %d\n", k )

	//  /* for 循环 */
	// for a := 0; a < 10; a++ {
	//     fmt.Printf("a 的值为: %d\n", a)
	// }
	// var a int
	// var b int = 15
	// var c int = 5
	// for a < b {
	//   a++
	//   var d = max(a,c)
	//   fmt.Printf("a 的值为: %d\n", a)
	//   fmt.Printf("d 的值为: %d\n", d)
	// }

	// numbers := []int{1, 2, 3, 5}
	// for i,x:= range numbers {
	//   fmt.Printf("第 %d 位 x 的值 = %d\n", i,x)
	// }

	//  var numbers []int
	// printSlice(numbers)

	// /* 允许追加空切片 */
	// numbers = append(numbers, 0)
	// printSlice(numbers)

	// /* 向切片添加一个元素 */
	// numbers = append(numbers, 1)
	// printSlice(numbers)

	// /* 同时添加多个元素 */
	// numbers = append(numbers, 2,3,4)
	// printSlice(numbers)

	// /* 创建切片 numbers1 是之前切片的两倍容量*/
	// numbers1 := make([]int, len(numbers), (cap(numbers))*2)

	// /* 拷贝 numbers 的内容到 numbers1 */
	// copy(numbers1,numbers)
	// printSlice(numbers1)

	nums := []int{2, 3, 4}
	sum := 0
	for _, num := range nums {
		sum += num
	}
	fmt.Println("sum:", sum)

	kvs := map[string]string{"a": "apple", "b": "banana"}
	for k, v := range kvs {
		fmt.Printf("%s -> %s\n", k, v)
	}
	//range也可以用来枚举Unicode字符串。第一个参数是字符的索引，第二个是字符（Unicode的值）本身。
	for i, c := range "go" {
		fmt.Println(i, c)
	}

	book1 := Books{"cat", "cooler", "test", 230}
	fmt.Println(book1.title)

}

func printSlice(x []int) {
	fmt.Printf("len=%d cap=%d slice=%v\n", len(x), cap(x), x)
}

type Books struct {
	title   string
	author  string
	subject string
	book_id int
}

/* 函数返回两个数的最大值 */
func max(num1, num2 int) int {
	/* 声明局部变量 */
	var result int

	if num1 > num2 {
		result = num1
	} else {
		result = num2
	}
	return result
}
