package main

import "fmt"

/* 函数返回两个数的最大值 */
func max4(num1, num2 int) int {
	/* 声明局部变量 */
	var result int

	if num1 > num2 {
		result = num1
	} else {
		result = num2
	}
	fmt.Println("-----main-------")
	fmt.Println(result)
	fmt.Println("-----main-------")
	return result
}
