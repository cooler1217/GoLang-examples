package main

import (
	"fmt"
	"hello"
)

func main() {
	var a, b int = 5, 8
	var c = hello.max(a, b)
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println("-------main-------")
	fmt.Println(c)
	fmt.Println("-------main-------")
}
