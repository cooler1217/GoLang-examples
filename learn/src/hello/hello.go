package hello

import "fmt"

// fmt.Println("-------main-------")

/* 函数返回两个数的最大值 */
func Max(num1, num2 int) int {
	/* 声明局部变量 */
	var result int
	if num1 > num2 {
		result = num1
	} else {
		result = num2
	}
	fmt.Println("-----max-------")
	fmt.Println(result)
	fmt.Println("-----max-------")
	return result
}
