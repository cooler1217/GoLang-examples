package main

// 包引入和管理，必须和包的文件夹名称一致
import (
	"fmt"
	"learn/src/hello"
	"learn/src/utils"
	"learn/src/utils/cat"
)

func main() {
	fmt.Println("-------main-------")
	var a, b int = 5, 8
	var c = hello.Max(a, b)
	fmt.Println(c)
	fmt.Println("-------main-------")
	c = utils.Max(a+10, b)
	fmt.Println(c)
	c = utils.Max2(a+13, b)
	fmt.Println(c)
	c = cat.Max3(a+33, b)
	fmt.Println(c)
}
