#!/usr/bin/env bash

CURRENT_DIR=`pwd`
OLD_GO_PATH="$GOPATH" 
OLD_GO_BIN="$GOBIN"    
export GOPATH="$CURRENT_DIR/"
export GOBIN="$CURRENT_DIR/bin"
export PATH=$PATH:/usr/

