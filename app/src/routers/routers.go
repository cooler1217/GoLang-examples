package routers

import (
	. "app/src/apis" //api部分
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	router := gin.Default()
	//Hello World
	// router.GET("/", IndexApi)

	//api调用的部分
	router.GET("/home/api", GetApiHtml)
	router.GET("/api/jsondata", GetJsonData)
	router.GET("/api/xmldata", GetXmlData)
	router.GET("/api/yamldata", GetYamlData)
	router.GET("/api/paramsdata", GetParamsJsonData)
	return router
}
