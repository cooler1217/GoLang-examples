package main

import (
	. "app/src/routers" //api部分
	"net/http"
)

func main() {
	router := InitRouter()
	http.ListenAndServe(":8080", router)
}
